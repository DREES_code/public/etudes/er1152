# ER1152

Ce dossier fournit les programmes permettant de produire les illustrations de l'Études et résultats n° 1152 de la DREES : "Profils, niveaux de ressources et plans d’aide des bénéficiaires de l’allocation personnalisée d’autonomie à domicile en 2017".

Le programme sur R produit les tableaux et illustrations à partir de la base de données disponible au CASD ou la base de données anonymisées disponible en Open Data *(lien à venir)*. 
Concernant la base anonymisée, il ne produit que les tableaux et ilustrations issus à partir des données en France métropolitaine, contrairement à la base disponible au CASD qui permet aussi d'avoir des résultats en France entière.

Lien vers l'étude: https://drees.solidarites-sante.gouv.fr/etudes-et-statistiques/publications/etudes-et-resultats/article/profils-niveaux-de-ressources-et-plans-d-aide-des-beneficiaires-de-l-allocation : cadrage sur les bénéficiaires de l'APA à domicile en 2017
	
Présentation de la DREES : La Direction de la recherche, des études, de l'évaluation et des statistiques (DREES) est le service statistique ministériel des ministères sanitaires et sociaux, et une direction de l'administration centrale de ces ministères.
https://drees.solidarites-sante.gouv.fr/article/presentation-de-la-drees

Source de données : enquête RI APA 2017 (Drees). Traitements : Drees.
lien:https://drees.solidarites-sante.gouv.fr/sources-outils-et-enquetes/01-les-donnees-individuelles-sur-lallocation-personnalisee-dautonomie

Les programmes ont été exécutés pour la dernière fois avec le logiciel R version 3.6.3 , le 04/06/2021.
